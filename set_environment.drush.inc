<?php
/**
 * @file
 * set_environment.drush.inc
 *
 * Drush command.
 */

/**
 * Implements hook_drush_help().
 */
function set_environment_drush_help($command) {
  switch ($command) {
    case 'drush:set-environment':
      return dt('Set the environment customizations after deploying a live database.');
  }
}


/**
 * Implements hook_drush_command().
 */
function set_environment_drush_command() {

  $items['set-environment'] = array(
    'description' => dt('Set the environment customizations after deploying a live database.'),
    'options' => array(
      'force' => dt('Force setting the environment even if it has already been set previously.'),
      'delete-public-files' => dt('Delete all files in the public directory. Use with caution.'),
    ),
  );
  return $items;
}


/**
 * Callback form main Drush command functionality.
 */
function drush_set_environment() {

  set_environment_set_environment(TRUE, drush_get_option('force'), drush_get_option('delete-public-files'));

}
