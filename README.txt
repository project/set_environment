This module is a simple Drush command that help you set up your Drupal
development environment every time you deploy a Drupal database from
production.

You can set custom variables on your settings file (either inside your
sites/<environment>, in settings.local.php file or wherever):

For example:
<pre>
// Enable some useful modules.
$conf['set_environment_modules_enable'] = array(
  'stage_file_proxy',
  'devel',
);

// Disable Varnish module.
$conf['set_environment_modules_disable'] = array('varnish');

// Replace production domain with local
$conf['set_environment_domain_search']   = 'yourdomain.com';
$conf['set_environment_domain_replace']  = 'local.yourdomain.com';
</pre>

This module is very preliminary, feature requests are welcome.
